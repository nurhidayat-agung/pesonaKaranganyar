package com.gits.developer.pesonakaranganyar.databinding;
import com.gits.developer.pesonakaranganyar.R;
import com.gits.developer.pesonakaranganyar.BR;
import android.view.View;
public class LayoutPopupMapBinding extends android.databinding.ViewDataBinding  {

    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.iv_pop_up_marker, 1);
        sViewsWithIds.put(R.id.place_title, 2);
        sViewsWithIds.put(R.id.img_place, 3);
        sViewsWithIds.put(R.id.distance, 4);
        sViewsWithIds.put(R.id.img_time, 5);
    }
    // views
    public final android.widget.TextView distance;
    public final android.widget.ImageView imgPlace;
    public final android.widget.ImageView imgTime;
    public final android.widget.ImageView ivPopUpMarker;
    private final android.support.v7.widget.CardView mboundView0;
    public final android.widget.TextView placeTitle;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LayoutPopupMapBinding(android.databinding.DataBindingComponent bindingComponent, View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds);
        this.distance = (android.widget.TextView) bindings[4];
        this.imgPlace = (android.widget.ImageView) bindings[3];
        this.imgTime = (android.widget.ImageView) bindings[5];
        this.ivPopUpMarker = (android.widget.ImageView) bindings[1];
        this.mboundView0 = (android.support.v7.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.placeTitle = (android.widget.TextView) bindings[2];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    public boolean setVariable(int variableId, Object variable) {
        switch(variableId) {
        }
        return false;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    public static LayoutPopupMapBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static LayoutPopupMapBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot, android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<LayoutPopupMapBinding>inflate(inflater, com.gits.developer.pesonakaranganyar.R.layout.layout_popup_map, root, attachToRoot, bindingComponent);
    }
    public static LayoutPopupMapBinding inflate(android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static LayoutPopupMapBinding inflate(android.view.LayoutInflater inflater, android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(com.gits.developer.pesonakaranganyar.R.layout.layout_popup_map, null, false), bindingComponent);
    }
    public static LayoutPopupMapBinding bind(android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static LayoutPopupMapBinding bind(android.view.View view, android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/layout_popup_map_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new LayoutPopupMapBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}