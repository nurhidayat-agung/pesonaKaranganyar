// Generated code from Butter Knife. Do not modify!
package com.gits.developer.pesonakaranganyar.ui.plan;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.gaurav.cdsrecyclerview.CdsRecyclerView;
import com.gits.developer.pesonakaranganyar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PlanYourTrip_ViewBinding implements Unbinder {
  private PlanYourTrip target;

  @UiThread
  public PlanYourTrip_ViewBinding(PlanYourTrip target, View source) {
    this.target = target;

    target.mRecyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'mRecyclerView'", CdsRecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PlanYourTrip target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mRecyclerView = null;
  }
}
