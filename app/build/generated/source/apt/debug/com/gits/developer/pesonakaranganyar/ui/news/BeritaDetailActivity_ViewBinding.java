// Generated code from Butter Knife. Do not modify!
package com.gits.developer.pesonakaranganyar.ui.news;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.gits.developer.pesonakaranganyar.R;
import com.gits.developer.pesonakaranganyar.ui.CirclePagesIndicator;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BeritaDetailActivity_ViewBinding implements Unbinder {
  private BeritaDetailActivity target;

  @UiThread
  public BeritaDetailActivity_ViewBinding(BeritaDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public BeritaDetailActivity_ViewBinding(BeritaDetailActivity target, View source) {
    this.target = target;

    target.appBarLayout = Utils.findRequiredViewAsType(source, R.id.appbar, "field 'appBarLayout'", AppBarLayout.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.custom_toolbar, "field 'toolbar'", Toolbar.class);
    target.collapsingToolbarLayout = Utils.findRequiredViewAsType(source, R.id.collapsing, "field 'collapsingToolbarLayout'", CollapsingToolbarLayout.class);
    target.viewPager = Utils.findRequiredViewAsType(source, R.id.viewpager, "field 'viewPager'", ViewPager.class);
    target.circlePagesIndicator = Utils.findRequiredViewAsType(source, R.id.circle_indicator, "field 'circlePagesIndicator'", CirclePagesIndicator.class);
    target.tvContentNews = Utils.findRequiredViewAsType(source, R.id.tv_content_news, "field 'tvContentNews'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BeritaDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.appBarLayout = null;
    target.toolbar = null;
    target.collapsingToolbarLayout = null;
    target.viewPager = null;
    target.circlePagesIndicator = null;
    target.tvContentNews = null;
  }
}
