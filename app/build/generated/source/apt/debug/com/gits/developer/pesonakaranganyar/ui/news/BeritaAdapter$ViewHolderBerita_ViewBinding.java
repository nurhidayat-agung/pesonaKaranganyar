// Generated code from Butter Knife. Do not modify!
package com.gits.developer.pesonakaranganyar.ui.news;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.gits.developer.pesonakaranganyar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BeritaAdapter$ViewHolderBerita_ViewBinding implements Unbinder {
  private BeritaAdapter.ViewHolderBerita target;

  @UiThread
  public BeritaAdapter$ViewHolderBerita_ViewBinding(BeritaAdapter.ViewHolderBerita target,
      View source) {
    this.target = target;

    target.ivBeritaTop = Utils.findRequiredViewAsType(source, R.id.iv_berita_top, "field 'ivBeritaTop'", ImageView.class);
    target.tvBeritaHeader = Utils.findRequiredViewAsType(source, R.id.tv_berita_header, "field 'tvBeritaHeader'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BeritaAdapter.ViewHolderBerita target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivBeritaTop = null;
    target.tvBeritaHeader = null;
  }
}
