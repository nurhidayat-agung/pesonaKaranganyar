package com.gits.developer.pesonakaranganyar.network.service;

import com.gits.developer.pesonakaranganyar.model.help.HelpResponServer;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by kazt on 16/06/17.
 */

public interface HelpService {
    @GET("/api/eholiday/getbantuan")
    Call<HelpResponServer> getHelp();
}
